FROM node:alpine

# Create app directory
WORKDIR /usr/src/socketiochat

# Install app dependencies
COPY . /usr/src/socketiochat/
RUN npm install

ENTRYPOINT ["npm", "start"]

EXPOSE 3000